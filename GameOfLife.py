import numpy as np
import time

class gol:
  def __init__(self,shape:tuple, board = None, nac=3, nbc=3, ndc=2, seed=None):
    '''
    Inicializa una tabla del juego de la vida con una probeída por el usuario o creda aleatoriamente

    Parámetros
    ----------
    shape: tuple
      tamaño de la tabla
    board: array, opcional
      array 2-dimensional, favor no entregar shape si se entrega board
    nac: int,opcional
      proporción de células tipo a
    nbc: int, opcional
      proporción de células tipo b
    ndc: int, opcional
      proporción de células muertas
    seed: int, opcional
      semilla a usar para la generación del tablero
    '''
    self.shape = shape
    self.nac = nac
    self.nbc = nbc
    self.ndc = ndc
    self._seed = seed
    if board:
       self.board = board
    else:
       self.board = self.create_board()

  def create_board(self):
    """
    Crea una tabla si no fue proveída una
    """

    if self._seed is not None:
      np.random.seed(self._seed)

    board = []
    for i in range(0, self.shape[1]):
      board.append([np.random.choice([0] * self.ndc + [1] * self.nac + [2] * self.nbc) for _ in range(0, self.shape[0])])
    return board


  def get(self,x,y):
    """
    Devuelve los valores (x,y) de la tabla.
    """
    return self.board[y % len(self.board)][x % len(self.board[0])]
      
  def assign(self,x,y,value):
    """
    Asigna un valor a (x,y).
    """
    self.board[y % len(self.board)][x % len(self.board[0])] = value
  
  def count_neighbors(self,x,y):

    '''
    retorna el número de vecinos vivos que tiene cada celda y de cada tipo.
    '''

    neighborsA=[]
    neighborsB=[]
    vecinos=[
        self.get( x - 1, y-1),
        self.get(x-1, y),
        self.get( x-1, y+1),
        self.get(x, y-1),
        self.get( x, y+1),
        self.get(x+1, y-1),
        self.get( x + 1, y),
        self.get(x + 1, y+1),]


    for i in vecinos:
      if i==2:
        neighborsB.append(i)
      if i ==1:
        neighborsA.append(i)
    return sum(neighborsA), sum(neighborsB)//2

  
  def updateBoard(self):
      """Actualiza el tablero actual según las reglas"""
      for y in range(0, len(self.board)):
          for x in range(0, len(self.board[y])):
              num_neighbors_a = self.count_neighbors(x, y)[0]
              num_neighbors_b = self.count_neighbors(x, y)[1]
              is_alive_a = self.get(x, y) == 1
              is_alive_b = self.get(x, y) == 2
              is_dead = self.get(x,y) == 0

              #supervivencia
              if (2 <= num_neighbors_a <= 3  and num_neighbors_b <= 2 and is_alive_a):
                  self.assign(x, y, 1)
              elif (2 <= num_neighbors_b <= 3  and num_neighbors_a <= 2 and is_alive_b):
                  self.assign(x, y, 2)

              # Muerte
              elif is_alive_a and (num_neighbors_a>3 or num_neighbors_a<2 or num_neighbors_b>2):
                 self.assign(x,y,0)
              elif is_alive_b and (num_neighbors_b>3 or num_neighbors_b<2 or num_neighbors_a>2):
                 self.assign(x,y,0)

              #Nacimiento
              elif (num_neighbors_a == 3 and num_neighbors_b <=2 and is_dead):
                  self.assign(x,y,1)
              elif (num_neighbors_b==3 and num_neighbors_a <=2 and is_dead):
                  self.assign(x,y,2)

               #Cazador presa
              elif (num_neighbors_b ==2 and is_alive_a):
                 self.assign(x,y,2)
              elif (num_neighbors_a ==2 and is_alive_b):
                 self.assign(x,y,1)
              
              else:
                  self.assign(x, y, 0)
  
  def drawBoard(self):
    """
    dibuja el tablero actual ascii
    """
    res = ''
    for row in self.board:
        for col in row:
            if col == 1:
                res += '* '
            elif col==2:
                res += 'o'
            else:
                res += '  '
        res += '\n'
    print(res)
  
  def iterate(self,NUM_ITERATIONS,pretty = False):
    """
    actualiza NUM_ITERATIONS veces el tablero

    Parámeters
    ---------
    NUM_ITERATIONS: int
      número de iteraciones
    pretty: bool, opcional
      imprime cada iteración con drawBoard(), default = False
    """
    if pretty:
      for i in range(0, NUM_ITERATIONS):
        from IPython.display import clear_output

        print('Iteration ' + str(i + 1))
        self.drawBoard()
        self.updateBoard()
        time.sleep(0.3)
        clear_output(wait=True)
    else:   
      for i in range(0, NUM_ITERATIONS):
        self.updateBoard()

  
  def count(self,normalized = False):
    """
    Cuenta el número de células de cada tipo
    Parámetros
    ---------
    normalized : bool, optional
      Si verdadero, retorna el conteo normalizado
    """
    matrix = np.array(self.board)
    na = np.count_nonzero(matrix==1)
    nb = np.count_nonzero(matrix==2)
    N = self.shape[0]*self.shape[1]
    if normalized:
      return na/N,nb/N
    else:
       return na,nb
     