# Juego de la vida, variante de 2 especies y estimación de la fdp

### Autores: Samuel Durán Bustamante *samuel.duran@udea.edu.co*, Kevin Restrepo Tobón *kevin.restrepo@udea.edu.co*

Se genera una matriz con entradas: células muertas = 0,células vivas de la especie A = 1,células vivas de la especie B = 2 generados aleatoriamente, pero con cierta razon entre ellas para controlar la cantidad de población que se quiere de cada especie. Las condiciones de esta variante son las siguientes:

1. si $x_i==1$ y $2 \leq N_v^{A} \leq 3$ y $N_v^{B} \leq 2$ entonces $x_i=1$, si se tiene que $x_i$==2 $A \iff B$, esta es la condición de supervivencia.

2.  si $x_i==1$ y $ N_v^{A} > 3$ ó $N_v^{A} <2 $ ó $N_v^{B} >2$ entonces $x_i=0$, si se tiene que $x_i$==2 $A \iff B$, esta es la condición de muerte

3. si $x_i==0$ y $N_v^A == 3$ y $N_v^B \leq 2 $ entonces $x_i=1$, al hacer el cambio $A \iff B$ se hace la asignación $x_i=2$, a esta se le conoce como la condición de nacimiento

Se agregó como condición adicional la dinámica cazador-presa:

4. si $x_i == 1$ y $N_v^B == 2$ se hace la asignación $x_i=2$, si $x_i == 2$ y $N_v^A == 2$ se hace la asignación $x_i=1$

Para estimar la función densidad de probabilidad bidimensional $p(A,B)$ se grafica un histograma 2D que sigue una distribución $\phi'$

![alt text](GolHistogram.png)

se procede a generar un conjunto de números aleatorios como valores de prueba $\phi$ y se aplica el algoritmo de metropolis para hacer el estimado de la fdp.

![alt text](metropolisHistogram.png)